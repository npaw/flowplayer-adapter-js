## [6.8.2] - 2024-05-23
### Library
- Packaged with `lib 6.8.56`
- Packaged with `adapter-hlsjs 6.8.9`

## [6.8.1] - 2023-03-20
### Library
- Packaged with `lib 6.8.35`
- Packaged with `adapter-dashjs 6.8.2`
- Packaged with `adapter-html5 6.8.8`
- Packaged with `adapter-hlsjs 6.8.8`

## [6.8.0] - 2022-02-23
### Library
- Packaged with `lib 6.8.12`
- Packaged with `adapter-hlsjs 6.8.2`
- Packaged with `adapter-html5 6.8.2`

## [6.7.0] - 2021-04-13
### Library
- Packaged with `lib 6.5.31`
- Packaged with `adapter-dashjs 6.7.2`
- Packaged with `adapter-hlsjs 6.7.4`
- Packaged with `adapter-html5 6.7.10`
### New player support
- This new version is a breaking change supporting the new flowplayer based on mediaElement, usable with dash and hls plugins and capable of tracking its events and metadata like renditions and bitrate.

## [6.5.0] - 2019-08-23
### Library
- Packaged with `lib 6.5.13`
### Fixed
- Stop reported incorrectly for live content

## [6.3.0] - 2018-07-19
### Library
- Packaged with `lib 6.3.4`
