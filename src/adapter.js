var youbora = require('youboralib')
var html5 = require('./html5ext')
var hlsjs = require('youbora-adapter-hlsjs')
var dashjs = require('youbora-adapter-dashjs')

youbora.adapters.Flowplayer = youbora.Adapter.extend({

  constructor: function(player) {
    if (player.hls) {
      return new hlsjs(player.hls)
    } else if (player.dash) {
      return new dashjs(player.dash)
    } else {
      return new html5(player)
    }
  }

})

module.exports = youbora.adapters.Flowplayer
