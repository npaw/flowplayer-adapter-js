var youbora = require('youboralib')
var html5 = require('youbora-adapter-html5')
var manifest = require('../manifest.json')

youbora.adapters.html5flow = html5.extend({

  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  getPlayerVersion: function () {
    return flowplayer.version
  },

  getIsLive: function () {
    return this.player.opt('live')
  },

  getPlayerName: function () {
    return 'Flowplayer'
  },

  registerListeners: function () {
    this.monitorPlayhead(true, false)

    var ev = {}
    if (typeof flowplayer !== 'undefined' && flowplayer.events) {
      ev = flowplayer.events
    }

    this.refs = [
      [ev.SEEKING || 'seeking', this.seekingListener.bind(this)],
      [ev.PLAY || 'play', this.playListener.bind(this)],
      [ev.TIME_UPDATE || 'timeupdate', this.timeupdateListener.bind(this)],
      [ev.PAUSE || 'pause', this.pauseListener.bind(this)],
      [ev.PLAYING || 'playing', this.playingListener.bind(this)],
      [ev.ERROR || 'error', this.errorListener.bind(this)],
      [ev.SEEKED || 'seeked', this.seekedListener.bind(this)],
      [ev.ENDED || 'ended', this.endedListener.bind(this)],
      [ev.NON_RECOVERABLE_ERROR || 'error:fatal', this.fatalErrorListener.bind(this)]
    ]
    for (var i = 0; i < this.refs.length; i++) {
      this.player.addEventListener(this.refs[i][0], this.refs[i][1])
    }
  },

  unregisterListeners: function () {
    if (this.monitor) this.monitor.stop()
    if (this.player && this.refs) {
      for (var i = 0; i < this.refs.length; i++) {
        this.player.removeEventListener(this.refs[i][0], this.refs[i][1])
        delete this.refs[i]
      }
    }
  },

  fatalErrorListener(e) {
    this.errorListener(e)
    this.fireStop()
  }
})

module.exports = youbora.adapters.html5flow
